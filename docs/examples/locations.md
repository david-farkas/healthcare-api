# locations.get

Healthcare API method: projects.locations.get

See: https://cloud.google.com/healthcare/docs/reference/rest/v1beta1/projects.locations/get

```
from healthcare_api.client import Client
c = Client()

location = c.locations.get(name='projects/PROJECT_ID/locations/LOCATION')  # TODO: change PROJECT_ID and LOCATION
print(location)
```

# locations.list

Healthcare API method: projects.locations.list

See: https://cloud.google.com/healthcare/docs/reference/rest/v1beta1/projects.locations/list

```
from healthcare_api.client import Client
c = Client()

locations = c.locations.list(name='projects/YOUR_PROJECT_ID') # TODO: change YOUR_PROJECT_ID
# NOTE: locations is a generator
for location in locations:
    print(location)
```
