# coding: utf-8

"""Google Cloud Healthcare API client library."""

import io
import os

from setuptools import setup, find_packages

NAME = 'flywheel-healthcare-api'

INSTALL_REQUIRES = [
    'requests~=2.22.0',
    'google-api-python-client~=1.7.9',
    'google-auth~=1.6.3',
]

DEV_REQUIRES = [
    'pycodestyle~=2.4.0',
    'pylint~=2.3.0',
    'pytest~=3.6.0',
    'pytest-cov~=2.5.0',
    'pytest-mock~=1.10.4',
]

package_root = os.path.abspath(os.path.dirname(__file__))

readme_filename = os.path.join(package_root, 'README.md')
with io.open(readme_filename, encoding='utf-8') as readme_file:
    readme = readme_file.read()

version_filename = os.path.join(package_root, 'flywheel_healthcare_api', 'VERSION')
with open(version_filename) as f:
    version = f.read().strip()

setup(
    name=NAME,
    version=version,
    description="Google Cloud Healthcare API client library",
    author_email="support@flywheel.io",
    url="https://github.com/flywheel-io/healthcare-api",
    keywords=["Flywheel", "flywheel", "google", "healthcare", "api"],
    install_requires=INSTALL_REQUIRES,
    extras_require={
        'dev': DEV_REQUIRES,
        'dicomWeb': ['dicomweb_client']
    },
    packages=find_packages(),
    include_package_data=True,
    license="MIT",
    project_urls={
        'Source': 'https://github.com/flywheel-io/healthcare-api'
    },
    zip_safe=False,
    long_description=readme,
    long_description_content_type='text/markdown'
)
