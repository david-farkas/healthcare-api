import os
import types
import time

import google.auth
import google.auth.exceptions
import pydicom
import pytest
from googleapiclient.errors import HttpError

from flywheel_healthcare_api.client import GCPError

if not (os.getenv('PROJECT') and os.getenv('GCLOUD_AUTH_TOKEN')):
    pytest.skip('skipping integration tests duo to invalid configuration', allow_module_level=True)

def test_client_dir(client):
    required_args = [
        'locations',
        'datasets',
        'annotationStores',
        'annotations',
        'dicomStores',
        'fhirStores',
        'hl7V2Stores',
        'operations'
    ]

    for arg in required_args:
        assert arg in dir(client)

def test_locations(client, project):
    us_central_location_path = client.location_path(project, 'us-central1')
    locations = client.locations.list(name='projects/' + project)
    assert isinstance(locations, types.GeneratorType)
    assert us_central_location_path in [l['name'] for l in locations]

    location = client.locations.get(name=us_central_location_path)
    assert location['name'] == us_central_location_path

def test_datasets(client, project, randstr):
    dataset_1 = randstr()
    location = 'us-central1'
    dataset_parent = client.location_path(project, location)
    # create
    client.datasets.create(parent=dataset_parent, datasetId=dataset_1, body={})
    # list
    datasets = client.datasets.list(parent=dataset_parent)
    assert isinstance(datasets, types.GeneratorType)
    dataset_name = client.dataset_path(project, location, dataset_1)
    assert dataset_name in [ds['name'] for ds in datasets]
    # get
    ds = client.datasets.get(name=dataset_name)
    assert ds['name'] == dataset_name
    # delete
    client.datasets.delete(name=dataset_name)
    with pytest.raises(HttpError):
        client.datasets.get(name=dataset_name)

def test_dicom_stores(client, project, randstr):
    dataset_1 = randstr()
    location = 'us-central1'
    store_1 = randstr()
    dataset_parent = client.location_path(project, location)
    # create dataset
    client.datasets.create(parent=dataset_parent, datasetId=dataset_1, body={})
    store_parent = client.dataset_path(project, location, dataset_1)
    # create DICOM store
    client.dicomStores.create(parent=store_parent, dicomStoreId=store_1, body={})
    store_name = client.store_path(project, location, dataset_1, 'dicom', store_1)
    # list stores
    stores = client.dicomStores.list(parent=store_parent)
    assert isinstance(stores, types.GeneratorType)
    assert store_name in [s['name'] for s in stores]
    # get store
    store = client.dicomStores.get(name=store_name)
    assert store['name'] == store_name
    # delete dicom store
    client.dicomStores.delete(name=store_name)
    with pytest.raises(HttpError):
        client.dicomStores.get(name=store_name)
    # TODO: other methods?

def test_get_dicomweb_client(client, project, randstr):
    dataset_1 = randstr()
    location = 'us-central1'
    store_1 = randstr()
    dirpath = os.path.dirname(os.path.abspath(__file__))
    dataset_parent = client.location_path(project, location)
    # create dataset
    client.datasets.create(parent=dataset_parent, datasetId=dataset_1, body={})
    store_parent = client.dataset_path(project, location, dataset_1)
    # create DICOM store
    client.dicomStores.create(parent=store_parent, dicomStoreId=store_1, body={})
    store_name = client.store_path(project, location, dataset_1, 'dicom', store_1)
    # get the dicom web to upload file
    dicomweb = client.dicomStores.dicomWeb(store_name)
    instance_uids = []
    dcm_file = 'MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm'
    filename = os.path.join(dirpath, '..', 'data', 'DICOM', dcm_file)
    dataset = pydicom.dcmread(filename)
    dicomweb.store_instances(datasets=[dataset])
    studies = dicomweb.search_for_studies()
    assert len(studies) == 1

def test_hl7v2_messages(client, project, randstr):
    dataset_1 = randstr()
    location = 'us-central1'
    hl7_store = randstr()
    dataset_parent = client.location_path(project, location)
    # create dataset
    client.datasets.create(parent=dataset_parent, datasetId=dataset_1, body={})
    store_parent = client.dataset_path(project, location, dataset_1)
    # create store
    client.hl7V2Stores.create(parent=store_parent, hl7V2StoreId=hl7_store, body={})
    store_path = client.store_path(project, location, dataset_1, 'hl7v2', hl7_store)
    msg = 'TVNIfF5+XCZ8QXxTRU5EX0ZBQ0lMSVRZfEF8QXwyMDE4MDEwMTAwMDAwMHx8VFlQRV5Bf' + \
          'DIwMTgwMTAxMDAwMDAwfFR8MC4wfHx8QUF8fDAwfEFTQ0lJDUVWTnxBMDB8MjAxODAxMD' + \
          'EwNDAwMDANUElEfHwxNAExMTFeXl5eTVJOfDExMTExMTExXl5eXk1STn4xMTExMTExMTE' + \
          'xXl5eXk9SR05NQlI='
    msg = client.hl7V2Stores.messages.create(parent=store_path, body={'message': {'data': msg}})
    # get message
    msg_2 = client.hl7V2Stores.messages.get(name=msg['name'])
    assert msg['name'] == msg_2['name']
    # list messages
    messages = client.hl7V2Stores.messages.list(parent=store_path)
    assert isinstance(messages, types.GeneratorType)
    assert msg['name'] in messages
    # delete message
    client.hl7V2Stores.messages.delete(name=msg['name'])
    messages = client.hl7V2Stores.messages.list(parent=store_path)
    assert not list(messages)

def test_fhir_resources(client, project, randstr):
    resource_payload = {
        'birthDate': '1970-01-01',
        'gender': 'female',
        'name': [
            {
                'family': 'Smith',
                'given': [
                    'Darcy'
                ],
                'use': 'official'
            }
        ],
        'resourceType': 'Patient'
    }
    dataset_1 = randstr()
    location = 'us-central1'
    fhir_store = randstr()
    dataset_parent = client.location_path(project, location)
    # create dataset
    client.datasets.create(parent=dataset_parent, datasetId=dataset_1, body={})
    store_parent = client.dataset_path(project, location, dataset_1)
    # create store
    client.fhirStores.create(parent=store_parent, fhirStoreId=fhir_store, body={})
    store_path = client.store_path(project, location, dataset_1, 'fhir', fhir_store)
    # create resource
    resource = client.fhirStores.fhir.create(parent=store_path, type='Patient', body=resource_payload)
    resource_path = client.fhir_resource_path(project, location, dataset_1, fhir_store, resource['resourceType'], resource['id'])
    # read resource
    resource = client.fhirStores.fhir.read(name=resource_path)
    assert resource['name'] == resource_payload['name']
    # search for resource
    time.sleep(2)  # ??? somehow the search doesn't return results immediately
    resource_list = client.fhirStores.fhir.search(parent=store_path, params={'resourceType': 'Patient'})
    assert resource_list['entry'][0]['resource']['name'] == resource_payload['name']
    # delete resource
    client.fhirStores.fhir.delete(name=resource_path)
    with pytest.raises(HttpError):
        client.fhirStores.fhir.read(name=resource_path)
