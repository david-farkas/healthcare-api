from flywheel_healthcare_api.client import Client

def test_helper_methods():
    location = Client.location_path('project', 'location')
    assert location == 'projects/project/locations/location'

    dataset = Client.dataset_path('project', 'location', 'dataset')
    assert dataset == 'projects/project/locations/location/datasets/dataset'
