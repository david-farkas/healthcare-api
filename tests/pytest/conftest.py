import binascii
import os

import google.auth
import google.auth.exceptions
import pytest

from flywheel_healthcare_api import client as hc_client


@pytest.yield_fixture(scope='function')
def client(mocker):
    credentials = None
    try:
        credentials, _ = google.auth.default()
    except google.auth.exceptions.DefaultCredentialsError:
        pass

    c = hc_client.Client(token=os.getenv('GCLOUD_AUTH_TOKEN'), credentials=credentials)
    datasets = c.datasets
    mocked_create_dataset = mocker.patch.object(
        datasets, 'create', wraps=datasets.create)
    setattr(datasets, 'create', mocked_create_dataset)
    setattr(c, 'datasets', datasets)
    yield c
    # cleanup datasets created by this client
    for create_call in mocked_create_dataset.call_args_list:
        _, kwargs = create_call
        name = '{}/datasets/{}'.format(kwargs['parent'], kwargs['datasetId'])
        try:
            c.datasets.delete(name=name)
        except:
            pass


@pytest.fixture(scope='session')
def project():
    project = None
    try:
        _, project = google.auth.default()
    except google.auth.exceptions.DefaultCredentialsError:
        pass
    return os.getenv('PROJECT') or project


@pytest.fixture(scope='function')
def randstr(request):

    def randstr():
        """Return random string prefixed with test module and function name"""

        def clean(test_name):
            return test_name.lower().replace('test_', '').rstrip('_').replace('_', '-')

        module = clean(request.module.__name__)
        function = clean(request.function.__name__)
        prefix = module + '-' + function
        return prefix[:21] + '-' + binascii.hexlify(os.urandom(5)).decode('utf-8')

    return randstr
