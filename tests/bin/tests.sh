#!/usr/bin/env sh

set -eu
unset CDPATH
cd "$( dirname "$0" )/../.."


USAGE="
Usage:
    $0 [OPTION...] [[--] PYTEST_ARGS...]

Run healthcare-api integration tests and linting.

Options:
    -h, --help          Print this help and exit
    -s, --shell         Enter shell instead of running tests
    -l, --lint-only     Run linting only
    -L, --skip-lint     Skip linting
    -- PYTEST_ARGS      Arguments passed to py.test

"

main() {
    export PYTHONDONTWRITEBYTECODE=1
    local LINT_TOGGLE=

    while [ $# -gt 0 ]; do
        case "$1" in
            -h|--help)
                printf "$USAGE"; exit 0;;
            -s|--shell)
                exec sh;;
            -l|--lint-only)
                LINT_TOGGLE=true;;
            -L|--skip-lint)
                LINT_TOGGLE=false;;
            --)
                shift; break;;
            *)
                break;;
        esac
        shift
    done

    if [ "$LINT_TOGGLE" != true ]; then
        pytest tests/pytest --exitfirst --cov=flywheel_healthcare_api --cov-report term-missing:skip-covered "$@"
    fi

    if [ "$LINT_TOGGLE" != false ]; then
        log "INFO: Running pylint ..."
        pylint --rcfile tests/.pylintrc flywheel_healthcare_api

        log "INFO: Running pycodestyle ..."
        pycodestyle --max-line-length=150 --ignore=E402 flywheel_healthcare_api
    fi

}

log() {
    printf "\n%s\n" "$@" >&2
}


main "$@"