"""File cache module for Google Discovery."""

import os.path
import hashlib
import tempfile


class FileCache:
    """FileCache class."""

    @staticmethod
    def filename(url):
        """Generate filename from url."""
        return os.path.join(
            tempfile.gettempdir(),
            'google_api_discovery_' + hashlib.md5(url.encode()).hexdigest())

    def get(self, url):
        """Get cache for an url."""
        try:
            with open(self.filename(url), 'rb') as f:
                return f.read().decode()
        except FileNotFoundError:
            return None

    def set(self, url, content):
        """Save url contet into a cache file."""
        with tempfile.NamedTemporaryFile(delete=False) as f:
            f.write(content.encode())
            f.flush()
            os.fsync(f)
        os.rename(f.name, self.filename(url))
